import 'dart:math';

import 'package:flutter/material.dart';

class PageMakeFace extends StatefulWidget {
  const PageMakeFace({
    super.key,
  required this.friendName
  });

  final String friendName;
  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}

class _PageMakeFaceState extends State<PageMakeFace> {

  num eyesNumber2 =  Random().nextInt(2)+1 ;
  num eyesBrowNumber2 =  Random().nextInt(2)+1;
  num faceNumber2 =  Random().nextInt(1)+1;
  num noseNumber2 =  Random().nextInt(2)+1;
  num mouseNumber2 =  Random().nextInt(2)+1;



  void _eyesNumber2 (){
    print(eyesNumber2);
  }

  void _faceNumber (){
    print(faceNumber2);
  }
  void _noseNumber (){
    print(noseNumber2);
  }


  @override
  void initState(){
    super.initState();
    _eyesNumber2();
    _faceNumber();
    _noseNumber();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('결과 보기'),
      ),
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Container(
              child: Center(
               child:
               Stack(
                 children: [
                    Image.asset('assets/eyebrow$eyesBrowNumber2.png'),
                    Image.asset('assets/eyes$eyesNumber2.png'),
                    Image.asset('assets/face$faceNumber2.png'),
                    Image.asset('assets/nose$noseNumber2.png'),
                    Image.asset('assets/mouse$mouseNumber2.png')
                ],
              ),),
            ),
          ],
        ),
      )
    );
  }
}
