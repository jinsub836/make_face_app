import 'package:flutter/material.dart';
import 'package:make_face_app/pages/page_make_face.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {


  String friendName = '';

  void _friendName (){
    setState(() {
      friendName='';
    });
  }





  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('이름 입력'),
      ),
      body: _buildBody(context),
    );
  }

    Widget _buildBody(BuildContext context){
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            Container( width: 300, margin: EdgeInsets.only(top: 100),
              child: TextField(
                onChanged: (value){
                  setState(() {
                    friendName = value;
                  });},
                decoration: InputDecoration(
                  hintText:'이름을 입력하세요.',
                )
                ,),
            ),
            Container( margin: EdgeInsets.only(top: 60),
              child:
              OutlinedButton(
                  onPressed: ()
                  {Navigator.push(context,
                  MaterialPageRoute(
                      builder: (context) =>
                      PageMakeFace(friendName: friendName )));
                  }, child: Text(' 시작 ')),
            ),
            Container( width: 350, height: 390,
              child:Text(''),
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/startback.png'),
                fit:BoxFit.cover
              ),
            ),)
          ],
        ),
      ),
    );
   }
  }

